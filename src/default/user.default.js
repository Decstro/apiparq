import db from '../DB/db.js'

const uDefault = async () => {
    const userDefault = await db.Users.findOrCreate({
        where: 
            {
                id: 1
            },
        defaults: 
            {
                name: 'Juan David',
                lastName: 'Puello López',
                password: 'jfjr8grnr9e',
                address: 'Cartagena, Bolivar',
                city: 'Cartagena',
                longitude: 54.3232,
                latitude: 30.4833,
                estadogeo: 'F'
            }
        });
 }

export default uDefault;
