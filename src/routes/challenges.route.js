import express, { json } from 'express';
import fetch from 'node-fetch';
import db from '../DB/db.js';

const router = express()

router.use(json());

router.get('/getUsers', async (req, res) => {
  const result = await db.Users.findAll({
    attributes: {exclude: ['password']}
  });
  res.json(result);
});

router.post('/postUser', async (req, res) => {
    const { name, lastName, address, password, city, longitude, latitude } = req.body;
    try{
      const newUser = await db.Users.create({
        name,
        lastName,
        address, 
        password,
        city,
        longitude,
        latitude
      });
      res.json(`Usuario creado exitosamente`)
    } catch(err){
      console.error(err);
    }
});

  
router.get('/getUser/:id', async (req, res) => {
    const { id } = req.params; 
    try{
      const user = await db.Users.findOne({where:{id}});
      if(user) res.json(user);
      else res.status(404).json('Usuario no encontrado');
    } catch (err){
      console.error(err);
    } 
});

router.delete('/deleteUser/:id', async (req, res) => {
  const { id } = req.params;
  try{
    const user = await db.Users.findOne({where:{id}});
    if(user){
      res.json(`Se elimino el usuario ${user.name}`);
      await db.Users.destroy({where:{id}});
    } 
    else res.status(404).json('Usuario no encontrado');
  } catch (err){
    console.error(err);
  } 
});

router.put('/updateEstado/:id', async (req, res) => {
  const { id } = req.params;
  const api_key = 'AIzaSyAAoH6hkoSfgd1ggXB5o8sY7yG3nZ6nZx8';
  try{
    const user = await db.Users.findOne({where:{id}});
    if(user){
        const response = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${user.address}&key=${api_key}`);
        const body = await response.json();
        if(body.status == 'REQUEST_DENIED' || body.status == 'INVALID_REQUEST'){
          await db.Users.update({id}, {where: {estadogeo: 'F'}});
          res.json('Se le coloco al usuario F, porque no se pudo convertir la dirección');
        }else{
          await db.Users.update({id}, {where: {estadogeo: 'A'}});
          res.json('Se encontro la dirección, la operación fue un exito');
        }
    } 
    else res.status(404).json('Usuario no encontrado');
  } catch (err){
    console.error(err);
  } 
});

export default router;