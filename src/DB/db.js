import mariadb from 'mariadb';
import userDefault from '../default/user.default.js';
import pkg from 'sequelize';
const { Sequelize, DataTypes, QueryTypes} = pkg;
const sequelize = new Sequelize('sqlite::memory:');

const db = {};
const database = 'usuariosparq';

(async function (){
  try {
    const connection = await mariadb.createConnection({
      host: "127.0.0.1",
      database: database,
      user: "root",
      password: "5d3250b5e",
      port: 3307
    })

    await connection.query(`CREATE DATABASE IF NOT EXISTS \`${database}\`;`);
    
    const User = sequelize.define('user', {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      lastName: {
          type: DataTypes.STRING,
          allowNull: false
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      address: {
        type: DataTypes.STRING,
        allowNull: false
      },
      city: {
          type: DataTypes.STRING,
          allowNull: false
      },
      longitude: {
          type: DataTypes.FLOAT,
          allowNull: false
      },
      latitude: {
          type: DataTypes.FLOAT,
          allowNull: false
      },
      estadogeo: {
          type: DataTypes.STRING,
          defaultValue: "F"
      }
    });    
      
    db.Sequelize = Sequelize;
    db.sequelize = sequelize;
      
    //DB Tables
    db.Users = User;     
      
    await sequelize.sync({ force: false });
    userDefault();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
})()

export default db;