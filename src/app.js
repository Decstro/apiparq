import express, { json } from 'express';
import swaggerJsDoc from 'swagger-jsdoc';
import swaggerUI from 'swagger-ui-express';
import challengeRoutes from './routes/challenges.route.js';
const app = express();
const port = 3001

app.use(json());

// const swaggerOptions = require('./docs/swaggerOptions.js');
// const swaggerSpecs = swaggerJsDoc(swaggerOptions);
// app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

app.use('/challenges', challengeRoutes);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});