# Apiparq

Esta api tiene como objetivo de cumplir con un reto planteado y en el transcurso demostrar mis conocimientos en el manejo de node js, javascript, swagger, sequelize, maria db, sql.

## Installation

Usaremos el siguiente comando para instalar los paquetes necesarios para gestionar la api

```bash
npm install 
```
## Uso
Lo primero que debemos hacer para ejecutar la api, es correr maria db, para ello nos ubicamos en la consola de maria db y colocamos el siguiente comando:
```python
mariadbd.exe --console
```
En esta api se usa sequelize para conectarse a maria db y de esta forma realizar el CRUD requerido, por lo tanto el paso anterior es muy importante, desues de esto ejecutamos la api y para ello debemos usar el comando:
```python
nodemon src/app.js
```
O en su defecto ingresar en la carpeta src y ejecutar:
```bash
nodemon app.js
```

## Notas
Debido a que expire el tiempo de prueba gratuito para ejecutar la Api_Key necesaria para la api de geolocation de google maps el programa necesitara una nueva api_Key para funcionar correctamente.
